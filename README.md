A tool to easily download your favourite series from mangadex, because you shouldn't trust SAAS platforms to keep hosting media you care about forever.

Usage: run `mddl` to print help menu

### Config format

Will inevitably change as more config options are added, generated empty for you when the program is run for the first time

- manga IDs should be inserted as strings, not numbers.

  e.g,

`{ "archiveLocation": "/home/cho/myMangaCollection/", "manga": ["30461", ...] }`
