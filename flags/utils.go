package flags

import (
	"os"
	"path"
)

func getDefaultConfigPath() string {
	userConfigDir, _ := os.UserConfigDir()
	defaultConfigPath := path.Join(userConfigDir, "mddl", "config.json")

	return defaultConfigPath
}
