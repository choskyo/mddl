package flags

import (
	"time"

	"github.com/urfave/cli/v2"
)

const (
	Config    = "config"
	Verbose   = "verbose"
	DataSaver = "data-saver"
	Language  = "language"
	Manga     = "manga"
	Chapter   = "chapter"
	Runtime   = "runtime"
)

var VerboseFlag = &cli.BoolFlag{
	Name:    Verbose,
	Aliases: []string{"v"},
	Usage:   "print less important info, useful for debugging",
	Value:   false,
}

var ConfigFlag = &cli.StringFlag{
	Name:    Config,
	Aliases: []string{"c"},
	Usage:   "the config file `path` to use for this run",
	Value:   getDefaultConfigPath(),
}

var LanguageFlag = &cli.StringFlag{
	Name:    Language,
	Aliases: []string{"l"},
	Value:   "gb",
}

var MangaFlag = &cli.StringFlag{
	Name:     Manga,
	Aliases:  []string{"m"},
	Required: true,
}

var ChapterFlag = &cli.StringFlag{
	Name:    Chapter,
	Aliases: []string{"ch"},
}

var RuntimeFlag = &cli.TimestampFlag{
	Name:   "runtime",
	Hidden: true,
	Value:  cli.NewTimestamp(time.Now()),
	Layout: "2006-01-02 15-04-05",
}
