package fm

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path"

	"github.com/urfave/cli/v2"
	"gitlab.com/choskyo/mddl/api"
	"gitlab.com/choskyo/mddl/flags"
	"gitlab.com/choskyo/mddl/logging"
)

func DownloadPages(c *cli.Context, chapter api.Chapter) error {
	if c.Bool(flags.Verbose) {
		if err := logging.Log(c, fmt.Sprintf(
			"Downloading %v V%v C%v L%v",
			chapter.MangaTitle,
			chapter.Volume,
			chapter.Chapter,
			c.String(flags.Language),
		)); err != nil {
			return err
		}
	}

	chapter, err := api.GetChapter(chapter.ID)
	if err != nil {
		return err
	}

	chapterPath, err := GetChapterPath(c, chapter)
	if err != nil {
		return err
	}

	if err := CreateChapterPathIfNotExists(chapterPath); err != nil {
		return err
	}

	for _, page := range chapter.Pages {
		url := fmt.Sprintf("%v%v/%v", chapter.Server, chapter.Hash, page)

		res, err := http.Get(url)
		if err != nil {
			return err
		}
		defer res.Body.Close()

		pageFile, err := os.Create(path.Join(chapterPath, page))
		if err != nil {
			return err
		}
		defer pageFile.Close()

		if _, err := io.Copy(pageFile, res.Body); err != nil {
			return err
		}
	}

	return nil
}
