package fm

import (
	"os"
	"path"

	"github.com/urfave/cli/v2"
	"gitlab.com/choskyo/mddl/api"
	"gitlab.com/choskyo/mddl/config"
)

func CreateChapterPathIfNotExists(chapterPath string) error {
	if _, err := os.Stat(chapterPath); os.IsNotExist(err) {
		err := os.MkdirAll(chapterPath, os.ModePerm)
		if err != nil {
			return err
		}
	}

	return nil
}

func GetChapterPath(c *cli.Context, chapter api.Chapter) (string, error) {
	cfg, err := config.GetConfig(c)
	if err != nil {
		return "", err
	}

	return path.Join(cfg.ArchivePath, chapter.MangaTitle, chapter.Volume, chapter.Chapter), nil
}
