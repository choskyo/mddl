package get

import (
	"strconv"

	"github.com/urfave/cli/v2"
	"gitlab.com/choskyo/mddl/api"
	"gitlab.com/choskyo/mddl/flags"
	"gitlab.com/choskyo/mddl/fm"
	"gitlab.com/choskyo/mddl/helpers"
)

var Command = &cli.Command{
	Name:   "get",
	Usage:  "download missing chapters",
	Action: get,
	Flags: []cli.Flag{
		flags.VerboseFlag,
		flags.LanguageFlag,
		flags.MangaFlag,
		flags.ChapterFlag,
	},
}

func get(c *cli.Context) error {
	manga := c.String(flags.Manga)
	chapterID := c.String(flags.Chapter)

	if chapterID != "" {
		chapterIDInt, err := strconv.Atoi(chapterID)
		if err != nil {
			return err
		}

		chapter, err := api.GetChapter(chapterIDInt)
		if err != nil {
			return err
		}

		return getChapter(c, chapter)
	}

	chapters, err := api.GetMangaChapters(manga)
	if err != nil {
		return err
	}

	chapters, err = helpers.RemoveDownloadedChapters(c, chapters)
	if err != nil {
		return err
	}

	chapters = helpers.FilterLanguage(c, chapters)

	for _, chapter := range chapters {
		if err := getChapter(c, chapter); err != nil {
			return err
		}
	}

	return nil
}

func getChapter(c *cli.Context, chapter api.Chapter) error {
	if err := fm.DownloadPages(c, chapter); err != nil {
		return err
	}

	return nil
}
