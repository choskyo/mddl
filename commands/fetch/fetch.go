package fetch

import (
	"github.com/urfave/cli/v2"
	"gitlab.com/choskyo/mddl/api"
	"gitlab.com/choskyo/mddl/config"
	"gitlab.com/choskyo/mddl/flags"
	"gitlab.com/choskyo/mddl/fm"
	"gitlab.com/choskyo/mddl/helpers"
)

var Command = &cli.Command{
	Name:   "fetch",
	Usage:  "download missing chapters",
	Action: fetch,
	Flags: []cli.Flag{
		flags.ConfigFlag,
		flags.VerboseFlag,
		flags.LanguageFlag,
		flags.RuntimeFlag,
	},
}

func fetch(c *cli.Context) error {
	cfg, err := config.GetConfig(c)
	if err != nil {
		return err
	}

	for _, mangaID := range cfg.MangaIDs {
		chapters, err := api.GetMangaChapters(mangaID)
		if err != nil {
			return err
		}

		chapters, err = helpers.RemoveDownloadedChapters(c, chapters)
		if err != nil {
			return err
		}

		chapters = helpers.FilterLanguage(c, chapters)

		for _, chapter := range chapters {
			if err := fm.DownloadPages(c, chapter); err != nil {
				return err
			}
		}
	}

	return nil
}
