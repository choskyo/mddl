package helpers

import (
	"os"

	"github.com/urfave/cli/v2"
	"gitlab.com/choskyo/mddl/api"
	"gitlab.com/choskyo/mddl/flags"
	"gitlab.com/choskyo/mddl/fm"
)

func RemoveDownloadedChapters(c *cli.Context, chapters []api.Chapter) ([]api.Chapter, error) {
	newChapters := []api.Chapter{}

	for _, chapter := range chapters {
		chapterPath, err := fm.GetChapterPath(c, chapter)
		if err != nil {
			return []api.Chapter{}, err
		}

		if _, err := os.Stat(chapterPath); os.IsNotExist(err) {
			newChapters = append(newChapters, chapter)
		}
	}

	return newChapters, nil
}

func FilterLanguage(c *cli.Context, chapters []api.Chapter) []api.Chapter {
	targetLanguage := c.String(flags.Language)
	newChapters := []api.Chapter{}

	for _, chapter := range chapters {
		if chapter.Language == targetLanguage {
			newChapters = append(newChapters, chapter)
		}
	}

	return newChapters
}
