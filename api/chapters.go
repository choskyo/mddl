package api

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
)

type chapterResponse struct {
	Chapter Chapter `json:"data"`
}

func GetChapter(id int) (Chapter, error) {
	url := fmt.Sprintf("https://api.mangadex.org/v2/chapter/%v", id)

	res, err := http.Get(url)
	if err != nil {
		return Chapter{}, err
	}
	defer res.Body.Close()

	var chapter chapterResponse

	err = json.NewDecoder(res.Body).Decode(&chapter)
	if err != nil {
		return Chapter{}, err
	}

	chapter.Chapter.MangaTitle = strings.ToLower(chapter.Chapter.MangaTitle)

	if chapter.Chapter.Volume == "" {
		chapter.Chapter.Volume = "0"
	}

	return chapter.Chapter, nil
}
