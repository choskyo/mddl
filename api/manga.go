package api

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
)

type MangaChaptersResponse struct {
	Data struct {
		Chapters []Chapter `json:"chapters"`
	} `json:"data"`
}

func GetMangaChapters(mangaID string) ([]Chapter, error) {
	url := fmt.Sprintf("https://api.mangadex.org/v2/manga/%v/chapters", mangaID)

	res, err := http.Get(url)
	if err != nil {
		return []Chapter{}, nil
	}
	defer res.Body.Close()

	var parsedResponse MangaChaptersResponse

	err = json.NewDecoder(res.Body).Decode(&parsedResponse)
	if err != nil {
		return []Chapter{}, err
	}

	chapters := []Chapter{}

	for _, c := range parsedResponse.Data.Chapters {
		vol := c.Volume
		if vol == "" {
			vol = "0"
		}

		chapters = append(chapters,
			Chapter{
				ID:         c.ID,
				Chapter:    c.Chapter,
				Hash:       c.Hash,
				Language:   c.Language,
				Server:     c.Server,
				Volume:     vol,
				MangaTitle: strings.ToLower(c.MangaTitle),
			},
		)
	}

	return chapters, nil
}
