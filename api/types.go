package api

type Manga struct {
	ID          int      `json:"id"`
	Title       string   `json:"title"`
	AltTitles   []string `json:"alt"`
	Description string
	Artist      []string
	Author      []string
	Publication Publication
}

type Publication struct {
	Language    string
	Status      int
	Demographic int
}

type Chapter struct {
	ID         int      `json:"id"`
	Volume     string   `json:"volume"`
	MangaTitle string   `json:"mangaTitle"`
	Chapter    string   `json:"chapter"`
	Pages      []string `json:"pages"`
	Server     string   `json:"server"`
	Hash       string   `json:"hash"`
	Language   string   `json:"language"`
}
