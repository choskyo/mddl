package logging

import (
	"fmt"
	"os"
	"path"

	"github.com/urfave/cli/v2"
	"gitlab.com/choskyo/mddl/config"
	"gitlab.com/choskyo/mddl/flags"
)

func Log(c *cli.Context, msg string) error {
	cfg, err := config.GetConfig(c)
	if err != nil {
		return err
	}

	timestamp := c.Timestamp(flags.Runtime)

	logFilePath := path.Join(cfg.LogPath, timestamp.Format("2006-01-02_15-04-05"))

	if _, err := os.Stat(logFilePath); os.IsNotExist(err) {
		_, err := os.Create(logFilePath)
		if err != nil {
			return err
		}
	}

	file, err := os.OpenFile(logFilePath, os.O_APPEND|os.O_WRONLY, os.ModePerm)
	if err != nil {
		return err
	}
	defer file.Close()

	if _, err := file.WriteString(msg + "\n"); err != nil {
		return err
	}

	fmt.Println(msg)

	return nil
}
