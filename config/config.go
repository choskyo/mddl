package config

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path"

	"github.com/urfave/cli/v2"
	"gitlab.com/choskyo/mddl/flags"
)

type Config struct {
	MangaIDs    []string `json:"manga"`
	ArchivePath string   `json:"archiveLocation"`
	LogPath     string   `json:"logPath"`
}

func GetConfig(c *cli.Context) (Config, error) {
	configPath := c.String(flags.Config)
	configFolder := path.Dir(configPath)

	createIfNotExists(configPath, configFolder)

	return getConfig(configPath)
}

func createIfNotExists(configPath, configFolder string) error {
	if _, err := os.Stat(configFolder); os.IsNotExist(err) {
		err := os.MkdirAll(configFolder, os.ModePerm)
		if err != nil {
			return err
		}
	}

	if _, err := os.Stat(configPath); os.IsNotExist(err) {
		homeDir, err := os.UserHomeDir()
		if err != nil {
			return err
		}

		defaultArchivePath := path.Join(homeDir, "mddl", "manga")
		if err := os.MkdirAll(defaultArchivePath, os.ModePerm); err != nil {
			return err
		}

		defaultLogPath := path.Join(homeDir, "mddl", "logs")
		if err := os.MkdirAll(defaultLogPath, os.ModePerm); err != nil {
			return err
		}

		newConfig := Config{
			MangaIDs:    []string{"30461"},
			ArchivePath: defaultArchivePath,
			LogPath:     defaultLogPath,
		}

		cfgJSON, _ := json.Marshal(newConfig)

		configFile, err := os.Create(configPath)
		if err != nil {
			return err
		}
		defer configFile.Close()

		configFile.Write(cfgJSON)
	}

	return nil
}

func getConfig(configPath string) (Config, error) {
	file, err := os.Open(configPath)
	if err != nil {
		return Config{}, err
	}
	defer file.Close()

	dat, _ := ioutil.ReadAll(file)

	var cfg Config
	json.Unmarshal(dat, &cfg)

	return cfg, nil
}
