package main

import (
	"os"
	"time"

	"github.com/urfave/cli/v2"
	"gitlab.com/choskyo/mddl/commands/fetch"
	"gitlab.com/choskyo/mddl/commands/get"
)

func main() {
	mddl := &cli.App{
		Name:     "mddl",
		HelpName: "mddl",
		Compiled: time.Now(),
		Commands: []*cli.Command{
			fetch.Command,
			get.Command,
		},
	}

	mddl.Run(os.Args)
}
